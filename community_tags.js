(function ($) {
  Drupal.communityTags = {};

  Drupal.communityTags.checkPlain = function (text) {
    text = Drupal.checkPlain(text);
    return text.replace(/^\s+/g, '')
               .replace(/\s+$/g, '')
               .replace('\n', '<br />');
  }

  Drupal.communityTags.serialize = function (data, prefix) {
    prefix = prefix || '';
    var out = '';
    for (i in data) {
      var name = prefix.length ? (prefix +'[' + i +']') : i;
      if (out.length) out += '&';
      if (typeof data[i] == 'object') {
        out += Drupal.communityTags.serialize(data[i], name);
      }
      else {
        out += name +'=';
        out += encodeURIComponent(data[i]);
      }
    }
    return out;
  }

  Drupal.behaviors.communityTags = {
    sequence: 0,

    attach: function(context) {
      // Note: all tag fields are autocompleted, and have already been initialized at this point.
      $('input.form-tags', context).each(function () {
          // Hide submit buttons.
          $('input[type=submit]', this.form).hide();

          // Fetch settings.
          var nid = $('input[name=nid]', this.form).val();
          var o = Drupal.settings.communityTags['n_' + nid];
          var vid = $('input[name=vid]', this.form).val();
          var o = Drupal.settings.communityTags['n_' + nid]['v_' + vid];

          var form = $(this.form);
          $(form).data('o', o);

          // Show the textfield and empty its value.
          var textfield = $(this).val('').css('display', 'inline');

          // Prepare the add Ajax handler and add the button.
          var addHandler = function () {
            // Send existing tags and new tag string.
            Drupal.behaviors.communityTags.sendTags(form, o, textfield[0].value);

            // Add tag to local list
            o.tags.push(textfield[0].value);
            o.tags.sort(function (a,b) { a = a.toLowerCase(); b = b.toLowerCase(); return (a>b) ? 1 : (a<b) ? -1 : 0; });

            // store "o" where it's easy to get at
            $(form).data('o', o);

            updateList(form);

            // Clear field and focus it.
            textfield.val('').focus();
          };
          var button = $('<input type="button" class="form-button" value="'+ Drupal.communityTags.checkPlain(o.add) +'" />').click(addHandler);
          $(this.form).submit(function () { addHandler(); return false; });


          // Create widget markup.
          // @todo theme this.
          var widget = $('<div class="tag-widget"><ul class="inline-tags clearfix"></ul></div>');
          textfield.before(widget);
          widget.append(textfield).append(button);
          var list = $('ul', widget);

          updateList(form);
      });

      // update the cloud when community tags are updated
      $(document).bind('CT_UPDATE_TAGS', function(e, form, data) {
        updateList(form);
        // var form_tags = $('input.form-tags', form);
        // NULL;
      });

      // Prepare the delete Ajax handler.
      var deleteHandler = function(li, form) {
        var textfield = $('input.form-tags', form);

        // keep stored "o" up to date
        var o = $(form).data('o');

        // Remove tag from local list.
        var i = $(li).attr('key');
        o.tags.splice(i, 1);

        // keep stored "o" up to date
        $(form).data('o', o);

        updateList(form);

        // Send new tag list.
        Drupal.behaviors.communityTags.sendTags(form, o, '');

        // Clear textfield and focus it.
        textfield.val('').focus();
      }

      function updateList(form) {
        var list = $('.tag-widget ul', form);
        var o = $(form).data('o');
        list.empty();
        for (i in o.tags) {
          list.append('<li key="'+ Drupal.communityTags.checkPlain(i) +'">'+ Drupal.communityTags.checkPlain(o.tags[i]) +'</li>');
        }
        $("li", list).hover(
          function () {
            $(this).addClass('hover');
          },
          function () {
            $(this).removeClass('hover');
          }
        );
        $('li', list).click(function() {
          deleteHandler(this, form)
        });
      }

    },


    sendTags: function(form, o, tag_to_add) {
      // Send existing tags and new tag string.
      $.post(o.url, Drupal.communityTags.serialize({ sequence: ++Drupal.behaviors.communityTags.sequence, tags: o.tags, add: tag_to_add, token: o.token }), function (data) {
        if (data.status && Drupal.behaviors.communityTags.sequence == data.sequence) {
          o.tags = data.tags;
          $(form).data('o', o);

          // notify that CT's have been updated
          $(document).trigger('CT_UPDATE_TAGS', [form, data]);
        }
      });
    },
  }

  Drupal.behaviors.communityTagsLinks = {
    attach: function(context) {
      // add checkboxes to all forms tags links display
      $('.community-tags-form').each(function() {
        addCheckboxes(this);
      });

      // update the cloud when community tags are updated
      $(document).bind('CT_UPDATE_TAGS', function(e, form, data) {
        if(data.cloud) {
          $('.ct-cloud', form).html(data.cloud);
          // put checkboxes back on
          addCheckboxes(form);
        }
      });

      // add checkboxes to links
      function addCheckboxes(form) {
        var o = $(form).data('o');

        $('.ct-cloud ul.inline-tags li a', form).each(function() {
          var tid = $(this).attr('id').substring(8);
          var inputId =  'ct-alltags-'+tid;
          // alert($.inArray(tid, o.tags));
          var tag = $(this).text();
          var is_checked = $.inArray(tag, o.tags) > -1;
          $(this).before($(document.createElement("input"))
          .attr({type: 'checkbox', id: inputId, checked: is_checked, value: tag})
          .change(function() {
            var o = $(form).data('o');
            if($(this).attr('checked')) {
              Drupal.behaviors.communityTags.sendTags(form, o, $(this).val());
            }
            else {
              var i = $.inArray($(this).val(), o.tags);
              o.tags.splice(i, 1);
              Drupal.behaviors.communityTags.sendTags(form, o, '');
            }
          }));
        });
      }
    }
  }
})(jQuery);